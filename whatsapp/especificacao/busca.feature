#language: pt

Funcionalidade: Realizar buscas de contatos


Cenario: Realizar busca na aba Chamadas
  Dado que aplicativo esteja aberto
  Quando eu clico obre a lupa
  E realizo uma busca por um contato
  Então devo ver todos as ultimas ligações desse do contato


Cenario: Realizar buscas na aba Conversas
  Dado que eu esteja na aba Conversas
  Quando eu clico obre a lupa
  E realizo uma busca por um contato
  Então devo ver a conversa do contato pesquisado


Cenario: Realizar buscas na aba Contatos
  Dado que eu esteja na aba Contatos
  Quando eu clico obre a lupa
  E realizo uma busca por um contato
  Então devo ver todos os contatos com o nome pesquisado

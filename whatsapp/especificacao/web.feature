#language: pt

Funcionalidade: Add / Remover computadores no WhatsApp Web
  Acessar no computador a qual deseja sincronizar a url: https://web.whatsapp.com/


Cenario: Add computador no aplicativo
  Dado que aplicativo esteja aberto
  Quando acesso menu do aplicativo
  E clico em WhatsApp Web
  E em seguida clico no botão '+'
  E aponto o celular para tela do computador para realizar a leitura do QRCode
  Então devo ter acesso ao WhatsApp na tela do Computador


Cenario: Excluir computador no aplicativo
  Dado que esteja na opção WhatsApp Web
  Quando e seguida clico na opção Sair de todos os computadores
  Então a lista de sessões ativas fica vazia e nenhum computador deve ter acesso ao WhatsApp

#language: pt

Funcionalidade: Add / Excluir contatos
  Pra add e excluir de contatos usuario deve ser adm do grupo

Cenario: Add contato em um grupo

  Dado que aplicativo esteja aberto
  Quando clico no menu do aplicativo
  E em seguida clico em Novo Grupo
  E seleciono todos os contatos que desejo
  Então deve ser exibida tela de criação do nome do Groupo
  Quando informo nome do grupo
  E clico em confirmar criação do Grupo
  Então devo ser direcionado a tela de mensagens


Cenario: Excluir contato de um grupo

  Dado que esteja na lista contatos
  Quando acesso o grupo desejado
  E clico em detalhes do grupo
  E pesquiso contato que desejo excluir
  E clico sobre o contato
  E clico sobre a opção Remover do grupo
  Então eu retornar pra list] não devo ver o contato excluido



class CodeKata::CheckOut
  attr_accessor :rules, :scanned_items

  def initialize rules
    self.rules = rules
    self.scanned_items = []
  end

  def scan item
    self.scanned_items << item
  end

  def total
    cumulative_total = 0

    occurance_map.each_pair do |item, occurance|
      cumulative_total += price_for item, occurance
    end

    cumulative_total
  end

  private

  def price_for item, occurance
    rule_array_for(item, occurance).inject(0) do |sum, rule|
      sum += self.rules[item][rule]
      sum
    end
  end

  def occurance_map
    self.scanned_items.each_with_object(Hash.new(0)) { |i,counts| counts[i] += 1 }
  end

  def rule_array_for item, occurance
    _result = []
    rules = self.rules[item].keys.sort

    while occurance > 0 do
      _rules = rules.push(occurance).sort
      closest_rule = _rules[_rules.rindex(occurance) - 1]
      _result << closest_rule
      occurance -= closest_rule
    end

    _result
  end
end
